--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.8
-- Dumped by pg_dump version 9.4.8
-- Started on 2016-06-15 01:21:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE IF EXISTS bibliopdf;
--
-- TOC entry 2047 (class 1262 OID 19168)
-- Name: bibliopdf; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE bibliopdf WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE bibliopdf OWNER TO postgres;

\connect bibliopdf

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2048 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 1 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2050 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 19169)
-- Name: dadoscatalogo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dadoscatalogo (
    patrimonio integer NOT NULL,
    titulo text,
    autoria text,
    veiculo text,
    data_publicacao timestamp without time zone,
    nomearquivo text,
    nomeoriginalarquivo text
);


ALTER TABLE dadoscatalogo OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 19175)
-- Name: dadoscatalogo_patrimonio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dadoscatalogo_patrimonio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dadoscatalogo_patrimonio_seq OWNER TO postgres;

--
-- TOC entry 2051 (class 0 OID 0)
-- Dependencies: 174
-- Name: dadoscatalogo_patrimonio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dadoscatalogo_patrimonio_seq OWNED BY dadoscatalogo.patrimonio;

ALTER TABLE ONLY dadoscatalogo ALTER COLUMN patrimonio SET DEFAULT nextval('dadoscatalogo_patrimonio_seq'::regclass);

ALTER TABLE ONLY dadoscatalogo
    ADD CONSTRAINT patrimonio_pk PRIMARY KEY (patrimonio);

-- User Table
CREATE TABLE usuarios (
    usuario character varying(15) NOT NULL,
    senha character varying(256)
);


ALTER TABLE usuarios OWNER TO postgres;

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT usuario_pk PRIMARY KEY (usuario);

-- End of dump

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Inserted by Hguimaraes
INSERT INTO dadoscatalogo VALUES (0,'The Lord of the Rings', 'J. R. R. Tolkien', 'Record', TO_TIMESTAMP('05-05-1950', 'MM-DD-YYYY'), 'lor', 'lor.pdf');
INSERT INTO dadoscatalogo VALUES (1,'Harry Potter e a Pedra Filosofal', 'J. K. Rowlling', 'Rocco', TO_TIMESTAMP('05-05-1994', 'MM-DD-YYYY'), 'hppf', 'hppf.pdf');
INSERT INTO dadoscatalogo VALUES (2,'As crônicas de Gelo e Fogo', 'George R. R. Martin', 'Bantam Spectra', TO_TIMESTAMP('05-05-2000', 'MM-DD-YYYY'), 'got', 'cgf.pdf');
INSERT INTO dadoscatalogo VALUES (3,'O Grande Mentecapto', 'Fernando Sabino', 'Record', TO_TIMESTAMP('05-05-1980', 'MM-DD-YYYY'), 'gm', 'ogm.pdf');
INSERT INTO dadoscatalogo VALUES (4,'Introduction to Algorithms', 'Thomas H. Cormen', 'Thomas H. Cormen', TO_TIMESTAMP('05-05-1900', 'MM-DD-YYYY'), 'ita', 'ita.pdf');

SELECT setval('dadoscatalogo_patrimonio_seq', (SELECT MAX(patrimonio) FROM dadoscatalogo));