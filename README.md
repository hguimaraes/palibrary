## paLibrary

>[UFRJ - EEL418] Second homework of Advanced Programming at @ UFRJ.

### Development Environment

1. SO: Ubuntu 14.04 64bits
2. DB: Postgres 9.4.8
3. IDE: Netbeans

### Requirements

1. Website to signup/signin Users to Upload and Download PDF files.
2. Need to be implemented using the Restful architecture
3. User authentication using HTTPS

### Technology Stack

1. Spring Boot in Backend
2. Angular JS, HTML5, CSS3 and JS for Frontend
3. Materialize CSS for the style

### Enable SSL/HTTPS in Spring Boot 

1. https://nemerosa.ghost.io/2015/07/06/enabling-ssl-with-spring-boot/

tl;dr :

```shell-script
	keytool -genkey -alias palibrary -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650 -keypass palibrary -storepass palibrary
```

### Routes
* Note : * All the routes are unsecured (It's on my todo list)

> [Search Methods for Books - GET]

```shell-script
	/findById? => Parameters: [q]
	/findByTitle? => Parameters: [q]
	/findByAuthor? => Parameters: [q]
	/findByPublisher? => Parameters: [q]
	/findByReleaseDate? => Parameters: [q]
	/findByDateRange? => Parameters: [start, end]
	/books
```

> [Save Method for Books - POST]

```shell-script
	/saveBook? => Parameters: [title,author,publisher,realeaseDate,file]
```

### References

1. http://visual-meta.com/tech-corner/3rd-level-restful-api-with-spring.html
2. https://spring.io/guides/gs/accessing-data-rest/
3. http://www.petrikainulainen.net/programming/spring-framework/spring-data-jpa-tutorial-introduction-to-query-methods/
4. http://krescruz.github.io/angular-materialize/
5. http://stackoverflow.com/questions/18915075/java-convert-string-to-timestamp
6. http://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
7. http://stackoverflow.com/questions/26186261/how-do-you-open-a-pdf-in-a-new-tab-and-show-it-in-the-browser-dont-ask-to-down