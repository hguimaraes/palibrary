package me.hguimaraes.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author hguimaraes
 */
public class DateFormatUtil {
    public Timestamp dateStringToTimestamp(String date){
        Timestamp timestamp = null;
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date parsedDate = dateFormat.parse(date);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());
        }catch(Exception e){
            e.getStackTrace();
        }
        return timestamp;
    }
}
