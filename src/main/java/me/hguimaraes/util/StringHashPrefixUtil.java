package me.hguimaraes.util;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * @author hguimaraes
 */
public class StringHashPrefixUtil {
    private final SecureRandom random = new SecureRandom();

    public String randomGen() {
        return new BigInteger(130, random).toString(32);
    }
}
