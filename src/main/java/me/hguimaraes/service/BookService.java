package me.hguimaraes.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import me.hguimaraes.util.StringHashPrefixUtil;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author hguimaraes
 */
public class BookService {
    private String filePath;
    private String fileName;
    private final String PDF_FOLDER = "files";
            
    public boolean uploadFile(MultipartFile file){
        // Return status
        boolean status = false;
        // Random generate Hash prefix to avoid file collision
        String hashPrefix = new StringHashPrefixUtil().randomGen();
        
        this.fileName = file.getOriginalFilename();
        this.filePath = Paths.get(this.PDF_FOLDER, hashPrefix + this.fileName).toString();
        
        try{
            // Save the file locally
            BufferedOutputStream stream = new BufferedOutputStream(
                new FileOutputStream(new File(this.filePath))
            );
            stream.write(file.getBytes());
            stream.close();
            status = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } 
        
        return status;
    }
    
    /*public MultipartFile downloadFile(long id){
        return new MultipartFile();
    } */
    
    public String getFilePath(){
        return this.filePath;
    }
    
    public String getFileName(){
        return this.fileName;
    }
    
    public String getPDFFolder(){
        return this.PDF_FOLDER;
    }
}