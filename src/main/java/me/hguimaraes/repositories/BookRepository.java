package me.hguimaraes.repositories;

import java.sql.Timestamp;
import java.util.List;
import me.hguimaraes.model.Book;
import org.springframework.data.repository.CrudRepository;

/**
 * @author hguimaraes
 */

public interface BookRepository extends CrudRepository<Book, Long> {
    // Find a single book by his ID
    Book findByPatrimonio(long patrimonio);
    
    // Search books by the title
    List<Book> findByTitulo(String titulo);
    
    // Search books by author
    List<Book> findByAutoria(String autoria);
    
    // Search books by publisher
    List<Book> findByVeiculo(String veiculo);
    
    // Search books by release date
    List<Book> findByDataPublicacao(Timestamp date);
    
    // Search books in a Date range
    List<Book> findByDataPublicacaoBetween(Timestamp start, Timestamp end);
    
    //Return all the books from Database
    @Override
    List<Book> findAll();
    
    void deleteByPatrimonio(long patrimonio);
}