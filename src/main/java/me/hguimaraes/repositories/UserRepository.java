package me.hguimaraes.repositories;

import java.util.List;
import me.hguimaraes.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author hguimaraes
 */
 
public interface UserRepository extends CrudRepository<User, Long> {
    // Find a specific user by his username
    User findByUsuario(String user);
    
    // List all the users in the app
    @Override
    List<User> findAll();
}