package me.hguimaraes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author hguimaraes
 */

@Entity
@Table(name="usuarios")
public class User {
    @Id
    @Column(name = "usuario")
    private String usuario;
        
    @Column(name = "senha")
    private String senha;
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public User(){
    }
    
    public User(String username, String pass){
        this.usuario = username;
        this.senha = pass;
    }
}