package me.hguimaraes.model;

/**
 * @author hguimaraes
 */

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="dadoscatalogo")
@NamedQueries({
    @NamedQuery(name = "Book.findByTitulo", query = "SELECT d FROM Book d WHERE LOWER(d.titulo) LIKE LOWER(CONCAT('%',?1,'%'))"),
    @NamedQuery(name = "Book.findByAutoria", query = "SELECT d FROM Book d WHERE LOWER(d.autoria) LIKE LOWER(CONCAT('%',?1,'%'))"),
    @NamedQuery(name = "Book.findByVeiculo", query = "SELECT d FROM Book d WHERE LOWER(d.veiculo) LIKE LOWER(CONCAT('%',?1,'%'))")
})
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "patrimonio")
    private long patrimonio;
    
    @Column(name = "titulo")
    private String titulo;
    
    @Column(name = "autoria")
    private String autoria;
    
    @Column(name = "veiculo")
    private String veiculo;
    
    @Column(name = "data_publicacao")
    private Timestamp dataPublicacao;
    
    @Column(name = "nomearquivo")
    private String nomearquivo;
    
    @Column(name = "nomeoriginalarquivo")
    private String nomeoriginalarquivo;
    
    public void setPatrimonio(long value){
        this.patrimonio = value;
    }
    
    public long getPatrimonio(){
        return this.patrimonio;
    }
    
    public void setTitulo(String value){
        this.titulo = value;
    }
    
    public String getTitulo(){
        return this.titulo;
    }
    
    public void setAutoria(String value){
        this.autoria = value;
    }
    
    public String getAutoria(){
        return this.autoria;
    }
    
    public String getVeiculo(){
        return this.veiculo;
    }
    
    public void setDataPublicacao(Timestamp value){
        this.dataPublicacao = value;
    }
    
    public Timestamp getDataPublicacao(){
        return this.dataPublicacao;
    }
    
    public void setNomeArquivo(String value){
        this.nomearquivo = value;
    }
    
    public String getNomeArquivo(){
        return this.nomearquivo;
    }
    
    public void setNomeOriginalArquivo(String value){
        this.nomeoriginalarquivo = value;
    }
    
    public String getNomeOriginalArquivo(){
        return this.nomeoriginalarquivo;
    }
    
    public Book(){
    }
    
    public Book(String title, String author, 
            String publisher,Timestamp releaseDate,
            String originalName, String filePath)
    {
        this.titulo = title;
        this.autoria = author;
        this.veiculo = publisher;
        this.dataPublicacao = releaseDate;
        this.nomeoriginalarquivo = originalName;
        this.nomearquivo = filePath;
    }
}