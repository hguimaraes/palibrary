package me.hguimaraes.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.PersistenceException;
import me.hguimaraes.model.Book;
import me.hguimaraes.repositories.BookRepository;
import me.hguimaraes.service.BookService;
import me.hguimaraes.util.DateFormatUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author hguimaraes
 */

@RestController
@EnableAutoConfiguration
public class BookController {
    @Autowired
    private BookRepository bookRepository;
    
    //private final String crossOrigin = "https://localhost:3000";
    private final String crossOrigin = "*";
    /*
     * +--------------------------+
     * |       Search Methods     |
     * +--------------------------+
     */
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/findById", method=RequestMethod.GET)
    public @ResponseBody Book findById(@RequestParam(value="q",
            required=true) long queryParameter)
    {
        Book book = bookRepository.findByPatrimonio(queryParameter);
        return book;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/findByTitle", method=RequestMethod.GET)
    public @ResponseBody List<Book> findByTitle(@RequestParam(value="q",
            required=true) String queryParameter)
    {
        List<Book> books = bookRepository.findByTitulo(queryParameter);
        return books;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/findByAuthor", method=RequestMethod.GET)
    public @ResponseBody List<Book> findByAuthor(@RequestParam(value="q",
            required=true) String queryParameter)
    {
        List<Book> books = bookRepository.findByAutoria(queryParameter);
        return books;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/findByPublisher", method=RequestMethod.GET)
    public @ResponseBody List<Book> findByPublisher(@RequestParam(value="q",
            required=true) String queryParameter)
    {
        List<Book> books = bookRepository.findByVeiculo(queryParameter);
        return books;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/findByReleaseDate", method=RequestMethod.GET)
    public @ResponseBody List<Book> findByReleaseDate(@RequestParam(value="q",
            required=true) Timestamp queryParameter)
    {
        List<Book> books = bookRepository.findByDataPublicacao(queryParameter);
        return books;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/findByDateRange", method=RequestMethod.GET)
    public @ResponseBody List<Book> findByDateRange(
            @RequestParam(value="start",required=true) Timestamp start,
            @RequestParam(value="end",required=true) Timestamp end)
    {
        List<Book> books = bookRepository.findByDataPublicacaoBetween(start, end);
        return books;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/books", method=RequestMethod.GET)
    public @ResponseBody List<Book> findAll(){
        List<Book> books = bookRepository.findAll();
        return books;
    }
    
    /*
     * +--------------------------+
     * |    Save/Upload Method    |
     * +--------------------------+
     */
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/saveBook", method=RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> saveBook(
        @RequestParam(value="title",required=true) String title,
        @RequestParam(value="author",required=true) String author,
        @RequestParam(value="publisher",required=true) String publisher,
        @RequestParam(value="realeaseDate",required=true) String releaseDate,
        @RequestParam(value="file",required=true) MultipartFile file)
    {
        // Convert the String releaseDate to TimeStamp format
        Timestamp bookDate = new DateFormatUtil().dateStringToTimestamp(releaseDate);
        
        BookService bookService = new BookService();
        
        if (bookService.uploadFile(file)){    
            // Filename after save in disk to in DB
            String filePath = bookService.getFilePath();
            String originalName = bookService.getFileName();
            
            // Create the book object
            Book book = new Book(title, author, publisher, 
                bookDate, originalName, filePath);
            
            try{                
                bookRepository.save(book);
            } catch(PersistenceException e) {
                e.getStackTrace();
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/downloadPDF", method=RequestMethod.GET, produces = "application/pdf")
    public @ResponseBody ResponseEntity<InputStreamResource> downloadPDF(@RequestParam(value="q",
            required=true) long queryParameter) throws IOException
    {
        Book book = bookRepository.findByPatrimonio(queryParameter);
        FileSystemResource pdfFile = new FileSystemResource(book.getNomeArquivo());
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("content-disposition", "inline; filename=" + book.getNomeArquivo());
        
        return ResponseEntity
            .ok()
            .header(headers.toString())
            .contentLength(pdfFile.contentLength())
            .contentType(MediaType.parseMediaType("application/pdf"))
            .body(new InputStreamResource(pdfFile.getInputStream()));
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/deleteById", method=RequestMethod.GET)
    public @ResponseBody boolean deleteById(@RequestParam(value="q",
            required=true) long queryParameter)
    {
        bookRepository.deleteByPatrimonio(queryParameter);
        return true;
    }
}