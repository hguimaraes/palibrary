package me.hguimaraes.controller;

import java.util.List;
import javax.persistence.PersistenceException;
import me.hguimaraes.model.User;
import me.hguimaraes.repositories.UserRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hguimaraes
 */

@RestController
@EnableAutoConfiguration
public class UserController {
    @Autowired
    private UserRepository userRepository;
    
    //private final String crossOrigin = "https://localhost:3000";
    private final String crossOrigin = "*";
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/login", method=RequestMethod.POST)
    public @ResponseBody boolean authenticate(@RequestBody User user)
    {
        boolean status = false;
        User userObj = userRepository.findByUsuario(user.getUsuario());
        
        // Check if the password match
        if(userObj != null && BCrypt.checkpw(user.getSenha(), userObj.getSenha())){
            status = true;
        }
        
        return status;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/users", method=RequestMethod.GET)
    public @ResponseBody List<User> findAll()
    {
        List<User> users = userRepository.findAll();
        return users;
    }
    
    @CrossOrigin(origins = crossOrigin)
    @RequestMapping(value = "/signup", method=RequestMethod.POST)
    public @ResponseBody boolean signup(@RequestBody User user)
    {
        boolean status = false;
        String hashedPass = BCrypt.hashpw(user.getSenha(), BCrypt.gensalt());
        User newUser = new User(user.getUsuario(), hashedPass);
        try{
            userRepository.save(newUser);
            status = true;
        } catch(PersistenceException e){
            e.getStackTrace();
        }
        return status;
    }
}